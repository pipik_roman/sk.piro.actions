/**
 * 
 */
package sk.piro.actions;

import org.junit.Test;

import sk.piro.core.Callback;

/**
 * Test for actions
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class ActionsTest {
	private final Actions actions = new Actions(new TestHandler());

	@Test
	public void testBasic() {
		actions.start(new Action<Object>("AAA") {
			@Override
			public Object execute() throws Throwable {
				System.err.println("Executing action AAA");
				return null;
			}
		});
	}

	@Test
	public void testReset() {
		Action<?> a = new Action<Object>("Resetable action") {

			@Override
			public Object execute() throws Throwable {
				assert getState() == ActionState.RESET;
				return null;
			}
		};
		assert a.getState() == ActionState.INITIAL;
		actions.start(a);
		a.reset();
		assert a.getState() == ActionState.RESET;
		actions.start(a);
		actions.start(a);
	}

	@Test
	public void testCancel() {
		Action<?> a = new Action<Object>("Cancelled action", true) {
			@Override
			public Object execute() throws Throwable {
				cancel();
				assert getState() == ActionState.CANCELING;
				confirmCancel();
				assert getState() == ActionState.CANCELED;
				return null;
			}
		};
		actions.start(a);
		assert a.getState() == ActionState.CANCELED;

		Action<?> b = new Action<Object>("Cancelling action", true) {
			@Override
			public Object execute() throws Throwable {
				cancel();
				assert getState() == ActionState.CANCELING;
				return null;
			}
		};
		actions.start(b);
		assert b.getState() == ActionState.DONE;
	}

	@Test
	public void testFail() {
		Action<?> a = new Action<Object>("Failing action", true) {
			@Override
			public Object execute() throws Throwable {
				throw new NullPointerException();
			}
		};
		actions.start(a);
		assert a.getState() == ActionState.FAILED;
	}

	@Test
	public void testIsCanceling() throws AssertionError {
		Action<?> a = new Action<Object>("Cancelled action", false) {
			@Override
			public Object execute() throws Throwable {
				cancel();
				return null;
			}
		};
		actions.start(a);
		assert a.getState() == ActionState.FAILED;
	}

	@Test
	public void testIsCanceling2() throws AssertionError {
		Action<?> a = new Action<Object>("Cancelled action", false) {
			@Override
			public Object execute() throws Throwable {
				confirmCancel();
				return null;
			}
		};
		actions.start(a);
		assert a.getState() == ActionState.FAILED;
	}

	@Test
	public void nestedActions() throws AssertionError {
		final Action<?> a = new Action<Object>("Action A", false) {
			@Override
			public Object execute() throws Throwable {
				System.err.println("Nested actions: " + actions.getActions().size());
				assert actions.getAction() == this;
				return null;
			}
		};
		final Action<?> b = new Action<Object>("Action B", false) {
			@Override
			public Object execute() throws Throwable {
				assert actions.getAction() == this;
				actions.start(a);
				return null;
			}
		};

		Action<?> c = new Action<Object>("Action C", false) {
			@Override
			public Object execute() throws Throwable {
				assert actions.getAction() == this;
				actions.start(b);
				return null;
			}
		};
		actions.start(c);
		assert c.getState() == ActionState.DONE;
	}
}

class TestHandler extends ActionsHandler {
	@Override
	public void actionEnd(Actions actions, ActionI<?> action) {
		super.actionEnd(actions, action);
		System.err.println("Action end: " + action);
	}

	@Override
	public void actionStart(Actions actions, ActionI<?> action) {
		super.actionStart(actions, action);
		System.err.println("Action start: " + action);
	}

	@Override
	public void firstAction(Actions actions, ActionI<?> action) {
		super.firstAction(actions, action);
		System.err.println("First action: " + action);
	}

	@Override
	public void noMoreActions(Actions actions) {
		super.noMoreActions(actions);
		System.err.println("No more actions!");
	}

	@Override
	protected void prepare(Actions actions, ActionI<?> action, Callback callback) {
		System.err.println("Preparing: " + action);
		super.prepare(actions, action, callback);
	}

}
