package sk.piro.actions.observers;

import sk.piro.actions.ActionI;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ActionFailedObserver extends ActionObserver {
	public void failed(ActionI<?> action);
}
