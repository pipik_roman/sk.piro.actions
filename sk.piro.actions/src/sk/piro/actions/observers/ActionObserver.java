package sk.piro.actions.observers;

import sk.piro.core.observer.Observer;

/**
 * Interface for identification of action observers
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ActionObserver extends Observer {
	//
}
