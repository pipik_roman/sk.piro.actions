package sk.piro.actions.observers;

import sk.piro.actions.ActionI;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ActionStartObserver extends ActionObserver {
	public void start(ActionI<?> action);
}
