package sk.piro.actions.observers;

import sk.piro.actions.ActionI;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ActionMessageObserver extends ActionObserver {
	public void messageChanged(ActionI<?> action);
}
