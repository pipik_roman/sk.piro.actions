/**
 * 
 */
package sk.piro.actions.observers;

import sk.piro.actions.ActionI;
import sk.piro.actions.Actions;
import sk.piro.core.observer.Observer;

/**
 * Observer of actions. Observers methods should not block execution!
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ActionsObserver extends Observer {

	/**
	 * Invoked before action starts. Action can be cancelled at this point to not run it
	 * 
	 * @param actions
	 *            actions manager
	 * @param action
	 *            action to start
	 */
	public void actionStart(Actions actions, ActionI<?> action);

	public void actionEnd(Actions actions, ActionI<?> action);

	/**
	 * Invoked when there are no actions and action is added.
	 * 
	 * @param actions
	 *            actions manager
	 * @param action
	 *            added action
	 */
	public void firstAction(Actions actions, ActionI<?> action);

	/**
	 * Invoked when all actions are done
	 * 
	 * @param actions
	 *            actions manager
	 */
	public void noMoreActions(Actions actions);
}
