package sk.piro.actions.observers;

import sk.piro.actions.ActionI;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ActionCancelingObserver extends ActionObserver {
	/**
	 * Invoked when action is canceling
	 * @param action canceling action
	 */
	public void canceling(ActionI<?> action);
}
