package sk.piro.actions.observers;

import sk.piro.actions.ActionI;

/**
 * Observer of action invoked when action is done, failed or cancelled
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public interface ActionEndedObserver extends ActionObserver {
	public void ended(ActionI<?> action);
}
