package sk.piro.actions;

/**
 * Enumeration of action states
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public enum ActionState {
	/**
	 * Initial state of action
	 */
	INITIAL,
	/**
	 * Close to {@link #INITIAL} but reseted after action has ended
	 */
	RESET,
	/**
	 * Actions is running
	 */
	RUNNING,
	/**
	 * Action is canceling but not yet canceled (it can still end successfully or fail if action do not know about cancel yet)
	 */
	CANCELING,
	/**
	 * Action ended by cancellation
	 */
	CANCELED,
	/**
	 * Action successfully ended
	 */
	DONE,
	/**
	 * Action ended by failure
	 */
	FAILED;

	public boolean isInitial() {
		switch (this) {
			case INITIAL:
				return true;
			default:
				return false;
		}
	}

	public boolean isReset() {
		switch (this) {
			case RESET:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Returns true if action is running or canceling.
	 * 
	 * @return true if action is running or canceling
	 * @see #isCanceling
	 *      isCanceling
	 */
	public boolean isRunning() {
		switch (this) {
			case RUNNING:
			case CANCELING:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Returns true if action is canceling but not canceled yet
	 * 
	 * @return true if action is canceling but not canceled yet
	 */
	public boolean isCanceling() {
		switch (this) {
			case CANCELING:
				return true;
			default:
				return false;
		}
	}

	public boolean isCanceled() {
		switch (this) {
			case CANCELED:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Returns true if action is DONE, CANCELED or FAILED
	 * 
	 * @return true if action is DONE, CANCELED or FAILED
	 */
	public boolean isEnded() {
		switch (this) {
			case CANCELED:
			case DONE:
			case FAILED:
				return true;
			default:
				return false;
		}
	}

	public boolean isFailed() {
		switch (this) {
			case FAILED:
				return true;
			default:
				return false;
		}
	}

	public boolean isDone() {
		switch (this) {
			case DONE:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Returns true if action was running and is done, canceled or failed
	 * 
	 * @return true if action was running and is done, canceled or failed
	 */
	public boolean wasRunning() {
		return isEnded();
	}

	public boolean canChangeTo(ActionState state) {
		assert state != null : "state cannot be null!";
		switch (this) {
			case INITIAL:
			case RESET:
				return state == RUNNING || state == ActionState.CANCELED;
			case RUNNING:
				return state == CANCELING || state == CANCELED || state == DONE || state == FAILED;
			case CANCELING:
				return state == CANCELED || state == DONE || state == FAILED;
			case FAILED:
			case DONE:
			case CANCELED:
				return state == RESET;
			default:
				throw new AssertionError();
		}
	}

	public void assertCanChangeTo(ActionState state) {
		assert this.canChangeTo(state) : "Cannot change actual state: " + this + " to state: " + state;
	}
}
