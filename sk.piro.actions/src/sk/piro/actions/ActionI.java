package sk.piro.actions;

import java.io.Serializable;

import sk.piro.actions.observers.ActionObserver;
import sk.piro.core.observer.Subject;

/**
 * Interface for action (without progress informations). Action should be reusable.
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <ResultType>
 *            result type of action.
 */
public interface ActionI<ResultType> extends Runnable, Serializable, Subject<ActionObserver> {

	/**
	 * Action execution method
	 * 
	 * @return return result data of action
	 * @throws Throwable
	 *             exception to throw. If this method throws it is set as failed
	 */
	public ResultType execute() throws Throwable;

	/**
	 * Cancel action from external source. Action has to confirm cancellation
	 */
	public void cancel();

	/**
	 * Returns true if action can be canceled
	 * 
	 * @return true if action can be canceled
	 */
	public boolean isCancelable();

	/**
	 * Returns true if action can be reseted
	 * 
	 * @return true if action can be reseted
	 */
	public boolean isResetable();

	/**
	 * Reset action
	 */
	public void reset();

	/**
	 * Called by {@link Actions} to prepare actions to start
	 */
	public void start();

	/**
	 * Called by {@link Actions} to fail action
	 * 
	 * @param t
	 *            exception of failure
	 */
	public void fail(Throwable t);

	/**
	 * If action is not canceling this will return false and do nothing. If action is canceling this will confirm cancel of action and return true. If action is canceled this will do nothing and
	 * return true
	 * 
	 * @return true if action is canceled now (has been canceling or canceled before this call)
	 */
	public boolean confirmCancel();

	/**
	 * Called by {@link Actions} and by {@link ActionI} implementation to set new state
	 * 
	 * @param state
	 *            state to set
	 */
	public void setState(ActionState state);

	/**
	 * Called by {@link Actions} to end action successfully
	 */
	public void done();

	/**
	 * Add action to default {@link Actions} and start it (useful if there is only one instance of {@link Actions})
	 * 
	 * @return this action
	 */
	public ActionI<ResultType> startInDefaultActions();

	/**
	 * Returns throwable causing failure of action or null
	 * 
	 * @return throwable causing failure of action or null
	 */
	public Throwable getThrowable();

	/**
	 * Returns result of action or null
	 * 
	 * @return result of action or null
	 */
	public ResultType getResult();

	/**
	 * Returns action start time as specified by {@link System#nanoTime()} or null
	 * 
	 * @return action start time as specified by {@link System#nanoTime()} or null
	 */
	public Long getStartTime();

	/**
	 * Returns action end time as specified by {@link System#nanoTime()} or null
	 * 
	 * @return action end time as specified by {@link System#nanoTime()} or null
	 */
	public Long getEndTime();

	/**
	 * Returns state of action
	 * 
	 * @return state of action
	 */
	public ActionState getState();

	/**
	 * Returns name of action
	 * 
	 * @return name of action
	 */
	public String getName();

	/**
	 * Returns message of progress
	 * 
	 * @return message of progress, or null if no message is set
	 */
	public String getMessage();

	/**
	 * Set message of progress
	 * 
	 * @param message
	 *            message of progress or null
	 */
	public void setMessage(String message);
}
