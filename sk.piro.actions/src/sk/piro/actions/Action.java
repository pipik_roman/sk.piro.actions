package sk.piro.actions;

import java.util.Set;

import sk.piro.actions.observers.ActionCanceledObserver;
import sk.piro.actions.observers.ActionCancelingObserver;
import sk.piro.actions.observers.ActionDoneObserver;
import sk.piro.actions.observers.ActionEndedObserver;
import sk.piro.actions.observers.ActionFailedObserver;
import sk.piro.actions.observers.ActionMessageObserver;
import sk.piro.actions.observers.ActionObserver;
import sk.piro.actions.observers.ActionResetObserver;
import sk.piro.actions.observers.ActionStartObserver;
import sk.piro.core.observer.InvalidObserverError;
import sk.piro.core.observer.ObserverRegistration;
import sk.piro.core.observer.ObserversBuilder;

/**
 * Action implementation
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <ResultType>
 *            type of object in result.
 */
public abstract class Action<ResultType> implements ActionI<ResultType> {
	private final String name;

	private ActionState state = ActionState.INITIAL;
	private String message = null;
	private Throwable throwable = null;
	private ResultType result = null;
	private Long startTime = null;
	private Long endTime = null;

	private final boolean isCancelable;
	private final boolean isResetable;

	protected Set<ActionObserver> startObservers = null;
	protected Set<ActionObserver> endedObservers = null;
	protected Set<ActionObserver> doneObservers = null;
	protected Set<ActionObserver> failedObservers = null;
	protected Set<ActionObserver> resetObservers = null;
	protected Set<ActionObserver> messageObservers = null;
	protected Set<ActionObserver> cancelingObservers = null;
	protected Set<ActionObserver> canceledObservers = null;

	/**
	 * Create action with no name, not cancelable but resetable
	 */
	public Action() {
		this(null);
	}

	/**
	 * Create action, not cancelable but resetable
	 * 
	 * @param name
	 *            name of action or null
	 */
	public Action(String name) {
		this(name, false, true);
	}

	/**
	 * Create action specified by {@link Runnable}
	 * 
	 * @param name
	 *            name of action
	 * @param isCancelable
	 *            true if action can be canceled
	 */
	public Action(String name, boolean isCancelable) {
		this(name, isCancelable, true);
	}

	/**
	 * @param name
	 *            name of action or null if not specified
	 * @param isCancelable
	 *            true if action can be canceled
	 * @param isResetable
	 *            true if action can be reseted
	 */
	public Action(String name, boolean isCancelable, boolean isResetable) {
		this.name = name;
		this.isCancelable = isCancelable;
		this.isResetable = isResetable;
	}

	/**
	 * This method cannot be overridden. Instead override method {@link #execute()}
	 */
	@Override
	public final void run() {
		try {
			result = execute();
		}
		catch (Throwable e) {
			fail(e);
		}
	}

	@Override
	public abstract ResultType execute() throws Throwable;

	@Override
	public ActionState getState() {
		return state;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;
		notifyMessageChanged();
	}

	private void notifyMessageChanged() {
		if (messageObservers == null) { return; }
		for (ActionObserver o : messageObservers) {
			((ActionMessageObserver) o).messageChanged(this);
		}
	}

	@Override
	public Long getEndTime() {
		return endTime;
	}

	@Override
	public Long getStartTime() {
		return startTime;
	}

	@Override
	public Throwable getThrowable() {
		return throwable;
	}

	@Override
	public ResultType getResult() {
		return result;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void cancel() {
		assert isCancelable : "Action is not cancelable!";
		assert getState().isRunning() : "Cannot cancel action if not running or cancelling";
		setState(ActionState.CANCELING);
		notifyCanceling();
	}

	private void notifyCanceling() {
		if (cancelingObservers == null) { return; }
		for (ActionObserver o : cancelingObservers) {
			((ActionCancelingObserver) o).canceling(this);
		}
	}

	@Override
	public boolean isCancelable() {
		return isCancelable;
	}

	/**
	 * Confirm cancel by action
	 */
	@Override
	public boolean confirmCancel() {
		assert isCancelable : "Action is not cancelable!";
		switch (state) {
			case CANCELED:
				return true;
			case RUNNING:
				return false;
			case CANCELING:
				setState(ActionState.CANCELED);
				setEndTime();
				notifyCanceled();
				return true;
			default:
				throw new AssertionError("Cannot confirm cancel in state: " + state);
		}
	}

	private void notifyCanceled() {
		if (canceledObservers == null) { return; }
		for (ActionObserver o : canceledObservers) {
			((ActionCanceledObserver) o).canceled(this);
		}
	}

	private void setEndTime() {
		this.endTime = System.nanoTime();
	}

	@Override
	public void setState(ActionState state) {
		this.getState().assertCanChangeTo(state);
		this.state = state;
	}

	@Override
	public void done() {
		setEndTime();
		setState(ActionState.DONE);
		notifyEnd();
		notifyDone();
	}

	private void notifyDone() {
		if (doneObservers == null) { return; }
		for (ActionObserver o : doneObservers) {
			((ActionDoneObserver) o).done(this);
		}
	}

	private void notifyEnd() {
		if (endedObservers == null) { return; }
		for (ActionObserver o : endedObservers) {
			((ActionEndedObserver) o).ended(this);
		}
	}

	@Override
	public void fail(Throwable t) {
		setState(ActionState.FAILED);
		throwable = t;
		setEndTime();
		notifyFailed();
	}

	private void notifyFailed() {
		if (failedObservers == null) { return; }
		for (ActionObserver o : failedObservers) {
			((ActionFailedObserver) o).failed(this);
		}
	}

	@Override
	public void start() {
		notifyStarted();
		setState(ActionState.RUNNING);
		startTime = System.nanoTime();
	}

	private void notifyStarted() {
		if (startObservers == null) { return; }
		for (ActionObserver o : startObservers) {
			((ActionStartObserver) o).start(this);
		}
	}

	@Override
	public boolean isResetable() {
		return this.isResetable;
	}

	@Override
	public void reset() {
		assert isResetable() : "Action" + this + " is not resetable";
		setState(ActionState.RESET);
		notifyReset();
	}

	private void notifyReset() {
		if (resetObservers == null) { return; }
		for (ActionObserver o : resetObservers) {
			((ActionResetObserver) o).reset(this);
		}
	}

	@Override
	public ObserverRegistration register(final ActionObserver observer) throws InvalidObserverError {
		ObserversBuilder<ActionObserver> b = new ObserversBuilder<ActionObserver>(observer);
		if (observer instanceof ActionCanceledObserver) {
			canceledObservers = b.add(canceledObservers);
		}
		if (observer instanceof ActionCancelingObserver) {
			cancelingObservers = b.add(cancelingObservers);
		}
		if (observer instanceof ActionDoneObserver) {
			doneObservers = b.add(doneObservers);
		}
		if (observer instanceof ActionEndedObserver) {
			endedObservers = b.add(endedObservers);
		}
		if (observer instanceof ActionFailedObserver) {
			failedObservers = b.add(failedObservers);
		}
		if (observer instanceof ActionMessageObserver) {
			messageObservers = b.add(messageObservers);
		}
		if (observer instanceof ActionResetObserver) {
			resetObservers = b.add(resetObservers);
		}
		if (observer instanceof ActionStartObserver) {
			startObservers = b.add(startObservers);
		}
		return b.create();
	}

	@Override
	public String toString() {
		return "Action [name=" + name + ", state=" + state + ", message=" + message + ", throwable=" + throwable + ", result=" + result + ", actionStart="
				+ startTime + ", actionEnd=" + endTime + ", isCancelable=" + isCancelable + ", isResetable=" + isResetable + "]";
	}

	@Override
	public Action<ResultType> startInDefaultActions() {
		Actions actions = Actions.getDefaultActions();
		assert actions != null : "Default actions should be set";
		return (Action<ResultType>) actions.start(this);
	}

	protected void actionStart() {
		this.startTime = System.nanoTime();
	}
}
