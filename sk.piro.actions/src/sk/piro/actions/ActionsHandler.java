package sk.piro.actions;

import sk.piro.actions.observers.ActionsObserver;
import sk.piro.core.Callback;

/**
 * Special case of {@link ActionsObserver} that manages actions. For example it creates wait dialog before executing action and closes dialog after closing action. Handler is first Observer to be
 * notified!
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class ActionsHandler implements ActionsObserver {
	/**
	 * Invoked by {@link Actions} before action starts
	 * 
	 * @param actions
	 *            action manager
	 * @param action
	 *            action to execute
	 */
	@Override
	public void actionStart(Actions actions, ActionI<?> action) {
		// to override
	}

	/**
	 * Invoked by {@link Actions} after action ended
	 * 
	 * @param actions
	 *            action manager
	 * @param action
	 *            ended action
	 */
	@Override
	public void actionEnd(Actions actions, ActionI<?> action) {
		// to override
	}

	/**
	 * Invoked by {@link Actions} when there are no actions and one action is added. This method provides callback to be invoked to enable using modal dialog
	 * 
	 * @param actions
	 *            action manager
	 * @param action
	 *            action that will be started
	 * @param callback
	 *            callback to invoke when handler is ready
	 */
	protected void prepare(Actions actions, ActionI<?> action, Callback callback) {
		callback.callback();
		// to override
	}

	/**
	 * Invoked by {@link Actions} when last action will end
	 * 
	 * @param actions
	 *            action manager
	 */
	@Override
	public void noMoreActions(Actions actions) {
		// to override
	}

	@Override
	public void firstAction(Actions actions, ActionI<?> action) {
		// to override
	}
}
