package sk.piro.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import sk.piro.actions.observers.ActionsObserver;
import sk.piro.core.Callback;
import sk.piro.core.observer.InvalidObserverError;
import sk.piro.core.observer.ObserverRegistration;
import sk.piro.core.observer.Subject;

/**
 * Actions manager
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class Actions implements Subject<ActionsObserver> {
	private static Actions defaultActions = null;

	/**
	 * Set default actions to use. This actions will be used in {@link ActionI#start}
	 * 
	 * @param defaultActions
	 *            default actions to use
	 */
	public static final void setDefaultActions(Actions defaultActions) {
		Actions.defaultActions = defaultActions;
	}

	/**
	 * Returns default actions
	 * 
	 * @return default actions
	 */
	public static Actions getDefaultActions() {
		return defaultActions;
	}

	private final List<ActionI<?>> actions = new ArrayList<ActionI<?>>();
	private final List<ActionI<?>> actionsGetter = Collections.unmodifiableList(actions);
	private final List<ActionsObserver> observers = new ArrayList<ActionsObserver>();

	private final ActionsHandler handler;

	/**
	 * Create action manager
	 * 
	 * @param handler
	 *            handler for actions
	 */
	public Actions(ActionsHandler handler) {
		if (defaultActions == null) {
			defaultActions = this;
		}
		this.handler = handler;
		register(handler);
	}

	/**
	 * Starts action. Action is ended after returning from it's run method. This method will block until action is not finished
	 * 
	 * @param action
	 *            action to start
	 * @return started action
	 */
	public <ResultType> ActionI<ResultType> start(ActionI<ResultType> action) {
		ActionState state = action.getState();
		assert !state.isRunning() : "Cannot start action in state: " + state;
		if (!(state.isInitial() || state.isReset())) {
			action.reset();
		}

		actions.add(action);
		if (actions.size() == 1) {
			handler.prepare(this, action, new Executor(action));
		}
		else {
			executeAction(action);
		}

		assert action.getState().isEnded() : "Action should be ended, but is in state:" + action.getState()
				+ " Did you forget to set dialog modal or forget to call executeAction in ActionHandler? Action: " + action.getName();
		return action;
	}

	/**
	 * Returns actual actions
	 * 
	 * @return actual actions
	 */
	public List<? extends ActionI<?>> getActions() {
		return actionsGetter;
	}

	/**
	 * Returns action currently on top (actual action)
	 * 
	 * @return action currently on top (actual action) or null if there are no actions
	 */
	public ActionI<?> getAction() {
		if (actions.isEmpty()) {
			return null;
		}
		else return actions.get(actions.size() - 1);
	}

	private void executeAction(ActionI<?> action) {
		assert this.actions.contains(action) : "Cannot execute action (" + action + ") that is not managed here";
		beforeExecution(action);
		try {
			if (!action.getState().isCanceled()) {
				action.run();
			}
		}
		catch (Throwable t) {
			action.fail(t);
		}
		finally {
			afterExecution(action);
		}
		assert action.getState().isEnded() : "Action should be ended at this point";
	}

	private void beforeExecution(ActionI<?> action) {
		action.start();
		for (ActionsObserver o : observers) {
			o.actionStart(this, action);
		}
	}

	private void afterExecution(ActionI<?> action) {
		actions.remove(action);
		switch (action.getState()) {
			case CANCELED:
			case FAILED:
				// nothing to invoke
				break;
			case RUNNING:
			case CANCELING: {
				action.done();
				break;
			}
			default:
				throw new AssertionError("Action cannot be in state: " + action.getState() + " at this point");
		}
		notifyEnd(action);
	}

	private void notifyEnd(ActionI<?> action) {
		for (ActionsObserver o : observers) {
			o.actionEnd(this, action);
		}

		if (getActions().isEmpty()) {
			for (ActionsObserver o : observers) {
				o.noMoreActions(this);
			}
		}
	}

	@Override
	public ObserverRegistration register(final ActionsObserver observer) throws InvalidObserverError {
		this.observers.add(observer);
		return new ObserverRegistration() {
			@Override
			public void remove() {
				observers.remove(observer);
			}
		};
	}

	private class Executor implements Callback {
		private final ActionI<?> action;

		protected Executor(ActionI<?> action) {
			super();
			this.action = action;
		}

		@Override
		public void callback() {
			executeAction(action);
		}
	}

}
